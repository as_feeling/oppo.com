-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2021-07-17 11:25:00
-- 服务器版本： 5.7.28
-- PHP 版本： 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `oppo.com`
--

-- --------------------------------------------------------

--
-- 表的结构 `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL COMMENT 'id',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '标题',
  `price` float NOT NULL COMMENT '价格',
  `limitBuy` int(11) NOT NULL COMMENT '限购',
  `number` int(11) NOT NULL COMMENT '数量',
  `pictureinfo` text COLLATE utf8_unicode_ci NOT NULL COMMENT '滚动图片',
  `pictureDetail` text COLLATE utf8_unicode_ci NOT NULL COMMENT '页面展示'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `product`
--

INSERT INTO `product` (`id`, `title`, `price`, `limitBuy`, `number`, `pictureinfo`, `pictureDetail`) VALUES
(1, '一加 OnePlus 9R 5G手机 蓝屿/8GB+256GB', 2999, 4, 999, '[{\"src\":\"../imgs/product-list/p1-00.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p1-01.jpeg\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p1-02.png\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p1-03.png\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p1-04.jpeg\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p1-05.png\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p1-06.jpeg\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p1-07.png\",\"alt\":\"small\"}]', '[{\"src\":\"../imgs/product-list/p1-11.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p1-12.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p1-13.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p1-14.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p1-15.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p1-16.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p1-17.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p1-18.png\",\"alt\":\"info\"}]'),
(2, 'OPPO Reno6 5G 星黛紫 8G+128G', 2799, 2, 999, '[{\"src\":\"../imgs/product-list/p2-00.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p2-01.jpeg\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p2-02.png\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p2-03.png\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p2-04.png\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p2-05.png\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p2-06.png\",\"alt\":\"small\"},{\"src\":\"../imgs/product-list/p2-07.png\",\"alt\":\"small\"}]', '[{\"src\":\"../imgs/product-list/p2-11.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p2-12.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p2-13.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p2-14.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p2-15.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p2-16.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p2-17.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p2-18.png\",\"alt\":\"info\"}]'),
(4, 'realme 真我GT Neo 闪速版 最终幻想/12GB+256GB', 2299, 5, 999, '[{\"src\":\"../imgs/product-list/p3-00.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-01.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-02.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-03.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-04.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-05.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-06.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-07.png\",\"alt\":\"info\"}]', '[{\"src\":\"../imgs/product-list/p3-11.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-12.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-13.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-14.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-15.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-16.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-17.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p3-18.png\",\"alt\":\"info\"}]'),
(5, 'OPPO Enco Free2 真无线主动降噪蓝牙耳机 星河白', 599, 99, 999, '[{\"src\":\"../imgs/product-list/p4-00.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-01.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-02.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-03.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-04.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-05.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-06.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-07.png\",\"alt\":\"info\"}]', '[{\"src\":\"../imgs/product-list/p4-11.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-12.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-13.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-14.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-15.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-16.jpeg\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-17.png\",\"alt\":\"info\"},{\"src\":\"../imgs/product-list/p4-18.jpeg\",\"alt\":\"info\"}]');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '手机号',
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '用户名',
  `county` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '国家'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`phone`, `password`, `username`, `county`) VALUES
('18267781878', 'root', 'root123', 'china'),
('13100000000', 'admin123!@', '13100000000', 'Afghanistan'),
('18266666666', 'admin123!@', '18266666666', 'Afghanistan'),
('18267781879', 'admin123!@', '18267781879', 'Afghanistan'),
('18729870192', 'admin123!@', '18729870192', 'China'),
('18267781110', '37994285Raj!@', '18267781110', 'Afghanistan');

--
-- 转储表的索引
--

--
-- 表的索引 `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`phone`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
