// import "./lib/jquery-3.6.0.min.js"
// import "./lib/bootstrap.js"

import "https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.js"
import "https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.0.2/js/bootstrap.min.js"
import "./lib/common.js"
import cookies from "./lib/cookies.js";

console.log($(window));

/**
 *
 * @param num
 * @param n
 * @returns n位，前补0
 */
function PrefixZero(num, n) {
    return (Array(n).join(0) + num).slice(-n);
}

/**
 *
 * @param hour  $hour 选择hour dom
 * @param minute    $minute 选择minute dom
 * @param second    $second 选择second  dom
 * @param now   nowDate
 * @param end   targetDate
 */
function Interval(hour, minute, second, now, end) {
    let time = end - now > 0 ? (end - now) / 1000 : false;
    // console.log(time)
    if (time <= 0) {
        return
    }
    let thisInterval = setInterval(function () {
        if (time > 0) {
            hour.html(PrefixZero(Math.floor(time / 60 / 60), 2));
            minute.html(PrefixZero(Math.floor(time / 60 % 60), 2));
            second.html(PrefixZero(Math.floor(time % 60), 2));
        } else {
            clearInterval(thisInterval);
        }
        time = time - 1;
    }, 1000);
}

function getFuture(timeList, length) {
    let slice = timeList.slice(4 - length);
    for (let i = 0; i < length; i++) {
        Interval(future_hour.eq(i), future_minute.eq(i), future_second.eq(i), now, slice[i]);
        // console.log(future_hour.eq(i))
    }
}

function getNowTime(timeList, index) {
    console.log(index)
    console.log(timeList);

    let d = new Date(timeList[index].getTime() + 1000 * 60 * 60 * 6)

    // console.log(p1.getTime())
    Interval(now_hour.eq(0), now_minute.eq(0), now_second.eq(0), now, d);
}

let killTime = $(".kill_time");

//test
let now = new Date();

let Compare_6 = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 6, 0, 0);
let p1=null,p2=null,p3=null,p4=null;


if (now<=Compare_6){
    console.log("yes");
    p1 = new Date(now.getFullYear(), now.getMonth(), now.getDate()-1, 24, 0, 0);
    p2 = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 6, 0, 0);
    p3 = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0);
    p4 = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 28, 0, 0);
    let timeLeft = $(".time_left h3");
    timeLeft.each(function (index,e){
        if (index==0){
            $(e).html("24:00");
        }else {
            $(e).html(index*6+":00");
        }
    })

} else {
    p1 = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 6, 0, 0);
    p2 = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0);
    p3 = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 18, 0, 0);
    p4 = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 24, 0, 0);
}

//以下作为选择倒数计时的元素
let now_hour = null;
let now_minute = null;
let now_second = null;




let timeList = [p1, p2, p3, p4];

for (let [index, value] of timeList.entries()) {
    // console.log(value-now,1000*60*60*6);
    if (value - now < 0 && value - now < -1000 * 60 * 60 * 6) {
        killTime.eq(index).removeClass(["now_time", "future_time"]).addClass("past_time");
        killTime.eq(index).find(".time_left span").css({display: "block"});
        killTime.eq(index).find(".time_left div").css({display: "none"});
        killTime.eq(index).find(".time_left span").html("已结束");
        killTime.eq(index).find(".time_right div").css({display: "none"});
        killTime.eq(index).find(".time_right > span").css({display: "block"});
        killTime.eq(index).find(".time_right").css({display: "block"});
        killTime.eq(index).find(".time_right").css({display: "block"});
        killTime.eq(index).removeClass("future_time").addClass("past_time");
    } else if (value - now > 0) {
        killTime.eq(index).removeClass("now_time", "past_time").addClass("future_time");
        killTime.eq(index).find(".time_left div").css({display: "block"});
        killTime.eq(index).find(".time_left > span").css({display: "none"});
        killTime.eq(index).find(".time_right").css({display: "none"});
    } else {
        // console.log(killTime.eq(index));
        killTime.eq(index).removeClass(["future_time", "past_time"]).addClass("now_time");
        killTime.eq(index).find(".time_left span").css({display: "block"});
        killTime.eq(index).find(".time_left div").css({display: "none"});
        killTime.eq(index).find(".time_right").css({display: "block"});
        killTime.eq(index).find(".time_right div").css({display: "block"});
        now_hour = $(".now_time").find(".time_right .hour");
        now_minute = $(".now_time").find(".time_right .minute");
        now_second = $(".now_time").find(".time_right .second");
    }
}
let futureTimeLength = $(".future_time").length;
let future_hour = $(".future_time").find(".time_left .hour");
let future_minute = $(".future_time").find(".time_left  .minute");
let future_second = $(".future_time").find(".time_left  .second");

getFuture(timeList, futureTimeLength);
getNowTime(timeList, 3 - futureTimeLength);
// console.log(futureTimeLength)

let aside = $("aside");
$(window).scroll(function () {
    if (document.documentElement.scrollTop > 760) {
        aside.css({
            position: "fixed",
            top: "140px"
        });
    } else {
        aside.css({
            position: "absolute",
            top: "950px"
        });
    }
})


// 渲染商品列表，给超链接转向
let img = $(".phone_product").find("img");
let img_a = $(".phone_product").find("a");

$.ajax({
    url: '../../interface/product.php',
    type: 'get', //GET
    timeout: 5000,    //超时时间
    dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
}).then(function (data) {
    // console.log(JSON.parse(data[0].pictureinfo)[0]);
    img.map(function (index, elem) {
        // console.log(JSON.parse(data[index].pictureinfo)[0]);
        $(elem).attr({src: JSON.parse(data[index].pictureinfo)[0].src})
        console.log($(elem),"此处为点击ajax导入的购物车信息");
    })

    img_a.map(function (index, elem) {
        $(elem).attr({href: "./product.html?id=" + data[index].id});
    })
    // console.log(6)


}).catch(function (xhr) {
    console.log(xhr.status)
})




