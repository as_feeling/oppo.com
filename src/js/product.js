import cookies from "./lib/cookies.js"
// import "./lib/jquery-3.6.0.min.js"
// import "./lib/bootstrap.js"

import "https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.js"
import "https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.js"
import "./lib/common.js"



//左边的滑动smallPic
let big = $(".ul_big");
let small = $("#smallPic");
let lt = $(".lt");
let gt = $(".gt");
let length = small.find("li").length;


//右边的滑动
let power = $(".power ul");
let pre = $(".pre");
let next = $(".next");
let length2 = power.find("li").length;


let smallImages = small.find("img");
let bigImages = big.find("img");


// product title
let title = $("#title");

// product price
let price = $("#price");

// product nav_body
let navBody = $(".nav_body").find("img");

//add to shopCar
let addShopCar = $("#addShopCar")[0];
// buy it right now 立即购买
let buyNow = $("#buyNow")[0];


// 限购
let limit = $("#limit");

//获取产品id
let id = location.search.split('=')[1];
// product number
let number = $("#number");

let cookiesPrice = null
let ajaxData = null



$.ajax({
    url: "../../interface/getItem.php",
    type: 'GET', //GET
    data: {id: id},
    // timeout: 5000,    //超时时间
    dataType: 'json'   //返回的数据格式：json/xml/html/script/jsonp/text
}).then(function (data) {
    ajaxData = data;
    if (data==null){
        location.href = "../html/index.html";
    }
    console.log(data);
    cookiesPrice = data.price;
    limit.html(data.limitBuy);

    // data = data[0];
    // 图片src  src:JSON.parse(data.pictureinfo)[index].src
    // title    data.title
    // console.log(JSON.parse(data.pictureinfo));
    smallImages.map(function (index, elm) {
        $(elm).attr({src: JSON.parse(data.pictureinfo)[index + 1].src});
    })
    bigImages.map(function (index, elm) {
        // console.log(JSON.parse(data.pictureinfo)[index].src)
        $(elm).attr({src: JSON.parse(data.pictureinfo)[index + 1].src});
    })
    navBody.map(function (index, elm) {
        // console.log(JSON.parse(data.pictureDetail)[index].src)
        $(elm).attr({src: JSON.parse(data.pictureDetail)[index].src});
    })

    title.html(data.title);
    price.text(data.price);
}).catch(function (xhr) {
    console.log(xhr.status);
})

$("#plus")[0].addEventListener('click',function (){
    if ((parseInt(number[0].value) + 1) > parseInt(limit[0].textContent)){
        number[0].value=limit[0].textContent;
    } else {
        number[0].value=parseInt(number[0].value) + 1;
    }
    $(".count_count").html(number[0].value+"件");
})

$("#mins")[0].addEventListener('click',function (){
    if (number[0].value==1){
        return
    } else {
        number[0].value=parseInt(number[0].value) - 1;
    }
    $(".count_count").html(number[0].value+"件");
})

number[0].addEventListener('change',function (){
    if (isNaN(this.value) && parseInt(this.value)<0){
        this.value = 1;
    } else if (parseInt(this.value)>ajaxData.limitBuy){
        this.value = parseInt(ajaxData.limitBuy);
    }
    this.value = parseInt(this.value);
    $(".count_count").html(number[0].value+"件");
})


addShopCar.addEventListener('click', function (e) {
    if (cookies.get('username')==''){
        alert("请先登陆");
        location.href = "../html/login.html";
    }
    addCookies(id, parseInt(number.val()), parseFloat(cookiesPrice));
    // window.open("./shoppingCar.html");
})
buyNow.addEventListener('click',function (e){
    if (cookies.get('username')==''){
        alert("请先登陆");
        location.href = "../html/login.html";
    }
    addCookies(id, parseInt(number.val()), parseFloat(cookiesPrice));
    window.open("./shoppingCar.html");
})


//购物车 addCookies
function addCookies(id, number, price) {
    let shop = cookies.get("shop");
    let item = {
        id,
        number,
        price
    };
    let addItem;

    if (shop) {
        let parse = JSON.parse(shop);
        // let filter = parse.filter(elem=>elem.id==item.id);
        // console.log(filter)
        let flag = 0;

        parse.forEach(function (elem, index) {
            if (elem.id == item.id) {
                if ((parseInt(elem.number)+item.number)>ajaxData.limitBuy){
                    elem.number = ajaxData.limitBuy;
                }else {
                    elem.number = parseInt(elem.number) + item.number
                }
                flag += 1;
            }
        });
        if (!flag) {
            parse.push(item);
        }
        addItem = JSON.stringify(parse);
    } else {
        let shopping = [];
        shopping.push(item)
        addItem = JSON.stringify(shopping);
    }
    cookies.set("shop", addItem);
    let newShop = cookies.get('shop');
    if (newShop!=''){
        let length = JSON.parse(cookies.get("shop")).length;
        $("#carCount").html(length);
    }
}


//防抖处理
function debounce(fn, time) {
    let timeout = null;
    return function () {
        if (timeout != null) clearTimeout(timeout);
        timeout = setTimeout(fn, time);
    }
}

function turnLeft() {
    let left = parseInt(getComputedStyle(small[0]).left);
    if (left == 0) {
        return
    }
    let computedStyle = left + 127;
    small.eq(0).css({left: computedStyle + "px"});
    if (computedStyle != 0) {
        gt.eq(0).css({opacity: 1});
    } else if (computedStyle == 0) {
        lt.eq(0).css({opacity: .6});
    }
}

function turnRight() {
    let right = parseInt(getComputedStyle(small[0], null).left);
    if (right == -(length - 4) * 127) {
        return
    }
    let computedStyle = right - 127;
    small.eq(0).css({left: computedStyle + "px"});
    if (computedStyle != -(length - 4) * 127) {
        lt.eq(0).css({opacity: 1});
    } else {
        gt.eq(0).css({opacity: .6});
    }
}


function turnLeftLi() {
    let left = parseInt(getComputedStyle(power[0]).left);
    if (left == 0) {
        return
    }
    let computedStyle = left + 120;
    power.eq(0).css({left: computedStyle + "px"});
    if (computedStyle != 0) {
        next.eq(0).css({visibility: "visible"});
    } else {
        pre.eq(0).css({visibility: "hidden"});
    }
}

function turnRightLi() {
    let right = parseInt(getComputedStyle(power[0]).left);
    console.log(getComputedStyle(power[0]).left);
    if (right == -(length2 - 5) * 120) {
        return
    }
    let computedStyle = right - 120;
    power.eq(0).css({left: computedStyle + "px"});
    if (computedStyle != -(length2 - 5) * 120) {
        pre.eq(0).css({visibility: "visible"});
    } else {
        next.eq(0).css({visibility: "hidden"});
    }
}

lt[0].addEventListener('click', debounce(turnLeft, 300))
gt[0].addEventListener('click', debounce(turnRight, 300))
pre[0].addEventListener('click', debounce(turnLeftLi, 300));
next[0].addEventListener('click', debounce(turnRightLi, 300));


//转移大尺寸图的位置
small[0].addEventListener('click', function (e) {
    if (e.target.nodeName == "LI") {
        return
    }
    $(e.target).parent().addClass("opacity1").siblings().removeClass("opacity1");
    let index = small.find("li").index($(e.target).parent()[0]);
    let left = -index * 600;
    big.eq(0).css({left: left});

    //点击事件相对位置的代码
    let position = $(e.target).parent().offset()['left'] - $(e.target).parent().parent().parent().offset()['left'];

    if (position <= 90) {
        lt.trigger('click');
    } else if (position >= 382) {
        gt.trigger('click');
    }
})

