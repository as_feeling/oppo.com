// import "./lib/jquery-3.6.0.min.js"

import "https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.js"
import "https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.0.2/js/bootstrap.min.js"
// import "./lib/jq"
import "./lib/select.js"




// import "./lib/common.js"
// import "./bootstrap.js"
// import "./bootstrap-select.min.js"
// import "./countrypicker.min.js"

// <script src="../js/jquery.min.js"></script>
// <script src="../js/select.js"></script>
// <!--	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-- >
// < script
// src = "../js/bootstrap.js"
// type = "text/javascript"
// charSet = "utf-8" > < /script>
// <!-- Latest compiled and minified JavaScript -->
// <script src="../js/bootstrap-select.min.js"></script>
// <script src="../js/countrypicker.min.js"></script>

let phoneNumber = $("#phoneNumber");
let getCode = $("#getCode");
let error = $(".error-number").eq(0);
let nocode = $(".nocode").eq(0);
let showCode = $(".show-code")[0];
let password = $("#password").eq(0);
let code = $(".code").eq(0);
let success = $(".btn-success");
let verification = $(".code")[0];
let wrong = $(".wrong").eq(0);
let check = $("#check");


$.ajax({
    url: '../../interface/checkNumber.php',
    type: 'get', //GET
    async: true,    //或false,是否异步
    data: {id:"1"},
    timeout: 5000,    //超时时间
    dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
    beforeSend: function (xhr) {
    },
    success: function (data, textStatus, jqXHR) {
        console.log(1)
    },
    error: function (xhr, textStatus) {
    }
})


// let county = $("#county");
phoneNumber[0].addEventListener('input', function (e) {
    if (this.value.length >= 6) {
        getCode.addClass("btn-color-green").removeClass("btn-color-gray");
    } else {
        getCode.addClass("btn-color-gray").removeClass("btn-color-green");
    }
})

let regExp = new RegExp(/^(?:(?:\+|00)86)?1[3-9]\d{9}$/);

getCode[0].addEventListener('click', function (e) {
    if ($(this).css("cursor") == 'not-allowed') {
        return
    }
    console.log(phoneNumber[0].value)
    // 1.先进行正则判断
    if (regExp.test(phoneNumber[0].value)) {
        error.css({display: "none"});
    } else {
        error.css({display: "block"});
        error.html("手机号码格式错误，请重新输入 (2310202)")
        return;
    }
    // 2.在进行ajax判断

    $.ajax({
        url: '../../interface/checkNumber.php',
        type: 'GET', //GET
        async: true,    //或false,是否异步
        data: {phoneNumber: phoneNumber[0].value},
        timeout: 5000,    //超时时间
        dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
        success: function (data, textStatus, jqXHR) {
        console.log(data)
            if (data != null) {
                error.css({display: "block"});
                error.html("手机号已被注册")
                return
            } else {
                error.css({display: "none"});
            }
            let count=10;
            getCode.addClass("btn-color-gray").removeClass("btn-color-green");

            nocode.css({display:"block"});
            code.removeAttr("disabled");

            let timeout = setInterval(function (){
                count--;
                getCode.eq(0).text(count+"s");
                if (count==0){
                    clearInterval(timeout);
                    getCode.eq(0).text("重新发送")
                    getCode.addClass("btn-color-green").removeClass("btn-color-gray");
                }
            },1000)
        },
        error: function (xhr, textStatus) {
            console.log(xhr.status);
        }
    })

    // $.ajax({
    //     url: '../../interface/checkNumber.php',
    //     type: 'get', //GET
    //     async: true,    //或false,是否异步
    //     data: {
    //         phoneNumber: phoneNumber[0].value
    //     },
    //     timeout: 5000,    //超时时间
    //     dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
    // }).then(function (data) {
    //     // console.log(data)
    //     if (data != null) {
    //         error.css({display: "block"});
    //         error.html("手机号已被注册")
    //         return
    //     } else {
    //         error.css({display: "none"});
    //     }
    //     let count=10;
    //     getCode.addClass("btn-color-gray").removeClass("btn-color-green");
    //
    //     nocode.css({display:"block"});
    //     code.removeAttr("disabled");
    //
    //     let timeout = setInterval(function (){
    //         count--;
    //         getCode.eq(0).text(count+"s");
    //         if (count==0){
    //             clearInterval(timeout);
    //             getCode.eq(0).text("重新发送")
    //             getCode.addClass("btn-color-green").removeClass("btn-color-gray");
    //         }
    //     },1000)
    // }).catch(function (xhr) {
    //     console.log(xhr.status);
    // });

})

showCode.addEventListener('click',function (e){
    if (password.attr("type")=='password'){
        password.attr({type:"text"});
    }else {
        password.attr({type:"password"});
        location.href
    }
})

success[0].addEventListener('click',function (){
    // let val = password.val();
    if ($(this).css("cursor") == 'not-allowed') {
        return
    }

    if (!checkCode(verification.value)){
        wrong.text("验证码有错，请输入6位数字的验证码")
        $(".code_error").css({display:"block"})
        return;
    }

    if (!checkPassword(password.val())) {
        wrong.text("密码格式错误,请输入6到16位密码 (2023)")
        $(".code_error").css({display: "block"})
        return
    }
    console.log($("form")[0])


    $("form")[0].submit();

})

/**
 * 以下代码为互相确认是否已经输入，并且恢复登陆按钮
 */

phoneNumber[0].addEventListener('input',checkBtn);

code[0].addEventListener('input',checkBtn);

password[0].addEventListener('input',checkBtn);

check[0].addEventListener('click',checkBtn)


/**
 * 做登录按钮颜色校验和cursor改变
 */
function checkBtn(){
    if (phoneNumber[0].value.length>=6 && code[0].value.length>0 && password[0].value.length>0 && check.prop("checked")){
        success.addClass("btn-success-yes").removeClass("btn-success-no");
        success.css({cursor:"pointer"});
    }else {
        success.addClass("btn-success-no").removeClass("btn-success-yes");
        success.css({cursor:"not-allowed"});
    }
}


/**
 * 正则校验 密码由 6-16 位数字、字母或符号组成、至少包含两种字符
 * @param string 需要验证的参数
 * @returns {boolean} 返回是否验证正确
 */
function checkPassword(string){
    // 密码由 6-16 位数字、字母或符号组成、至少包含两种字符
    let re = new RegExp(/^\S*(?=\S{6,16})(?=\S*\d)(?=\S*[A-Za-z])(?=\S*[!@#$%^&*? ]{2,})\S*$/);
    return  re.test(string);
}

/**
 *  正则校验 验证码由6为纯字符组成，这里不做验证码识别
 * @param string 需要验证的参数
 * @returns {boolean} 返回是否验证正确
 */
function checkCode(string){
    let re = new RegExp(/\d{6}/);
    return re.test(string);

}



// console.log(county)
// county[0].addEventListener('click',function (e){
//     console.log(1);
//     console.log($(".countrypicker").eq(0).trigger('click'));
//     // $(".countrypicker")[0].trigger('click');
// })


