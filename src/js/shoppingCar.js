import cookies from "./lib/cookies.js";

// import "https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.js"
// import "https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.0.2/js/bootstrap.min.js"

import "./lib/jquery-3.6.0.min.js"
import "./lib/bootstrap.js"
import "./lib/common.js"


let username = cookies.get("username");
if (username==''){
    location.href = "../html/index.html";
}

let shop = null;
let parse = null;
let productNumber = $("#product_number");

// 单个选中
let checkIt = null;

// 总价 summary money
let all = $("#all");

// 购物车里面的数量
let shopList = null;
let idList = null;

/**
 * 初始化内容 解决一个小bug
 *
 */
function init(){
    shop = cookies.get("shop");
    if (shop==''){
        return;
    }
    parse = JSON.parse(shop);
    idList = parse.map(function (elem) {
        return elem.id;
    }).join();
}

init();

$.ajax({
    url: '../../interface/shopCart.php',
    type: 'GET', //GET
    data: {
        idList
    },
    timeout: 5000,    //超时时间
    dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
}).then(function (data) {

    let temp = ``;
    parse.forEach(function (elem) {
        let filter = data.filter(e => e.id == elem.id);
        temp += `<li class="list" data-id="${elem.id}">
                <div class="table-info">
                <div class="checkbox-diy">
                <input type="checkbox" name="" id="" value="" class="checkIt" checked="checked"/>
                </div>
                <img src="${JSON.parse(filter[0].pictureinfo)[2].src}">
                </div>
                <div class="row">
                <div class="col col-md-5 product_info"><a href="">${filter[0].title}</a></div>
                <div class="col col-md-2 singlePrice">¥${filter[0].price}</div>
                <div class="col col-md-2">
                <div class="changeNmuber" ">
                <button type="button" class="btn btn-default minus" >-</button>
                <input type="" name="" id="" value="${elem.number}" class="product_number" />
                <button type="button" class="btn btn-default plus" >+</button>
                </div>
                <div class="limit">限购<span>${filter[0].limitBuy}</span>件</div>
                </div>
                <div class="col col-md-2 "><span>¥</span><span class="money">${filter[0].price * elem.number}</span></div>
                <div class="col col-md-1">
                <button type="button" class="btn btn-default delete">删除商品</button>
                <button type="button" class="btn btn-default">修改商品</button>
                </div>
                </div>
                </li>`;
    })
    shopList = $("#shopList");

    shopList.html(temp);
    productNumber.html($("#shopList .list").length);
    changeNum($(".plus"), 1);
    changeNum($(".minus"), -1);
    removeList($(".delete"));
    getSumMoney();

    // 单个选中
    checkIt = $(".checkIt");

    getCheckedSum(checkIt);
    // let product_number = $(".product_number");
    changeInput($(".product_number"));


}).catch(function (xhr) {
    console.log(xhr.status)
});


/**
 * changeNum 改变购物车的商品数量 并修改cookies的值
 * @param elem  给绑定按钮的点击事件
 * @param change 增减数值的大小
 */
function changeNum(elem, change) {
    change = parseInt(change);
    for (const tempElement of elem) {
        let limit = $(tempElement).parents(".row").find(".limit span")[0].textContent;
        tempElement.addEventListener('click', function (e) {
            let val = $(tempElement).parent().find("input")[0];
            if (parseInt($(tempElement).parent().find("input").val()) + change <= 0) {
                val.value = 1;
            } else if (parseInt($(tempElement).parent().find("input").val()) + change > limit) {
                return
            } else {
                val.value = parseInt($(tempElement).parent().find("input").val()) + change;
            }
            // 更改  cookies
            let map = parse.map(function (e) {
                if (e.id == $(val).parents(".list").attr("data-id")) {
                    e.number = val.value;
                }
                return e
            });
            let s = JSON.stringify(map);
            cookies.set("shop", s);
            $(this).parents(".row").find(".money").html(val.value * parseFloat($(this).parents(".row").find(".singlePrice")[0].textContent.slice(1)))

            //
            getSumMoney();  //获得商品总价
        })
    }
}

/**
 *  通过输入框来改变商品的数量 同时改变小计和总价
 * @param elem
 */
function changeInput(elem){
    for (const elemElement of elem) {
        let limit = $(elemElement).parents(".row").find(".limit span")[0].textContent;
        elemElement.addEventListener('change',function (e){
            if (this.value < 0){
                this.value = 0;
            } else if(parseInt(this.value) > limit){
                this.value = limit;
            } else {
                this.value = parseInt(this.value)
            }
            changeCookies(this,this)
            getSumMoney();  //获得商品总价

        })
    }
}


/**
 *  改变cookies里面的值 因为我有按钮和输入框需要更改
 * @param 选中的元素
 * @param 输入框的值
 */
function changeCookies(e,val){
    // 更改  cookies
    let map = parse.map(function (e) {
        if (e.id == $(val).parents(".list").attr("data-id")) {
            e.number = val.value;
        }
        return e
    });
    let s = JSON.stringify(map);
    cookies.set("shop", s);
    console.log(val.value)
    $(e).parents(".row").find(".money").html(val.value * parseFloat($(e).parents(".row").find(".singlePrice")[0].textContent.slice(1)))

}


// cookies remove
/**
 * cookies remove 在删除元素和改变数量的同时改变cookies里面的内容
 * @param elem 点击事件的当前li元素
 */
function cookiesRemove(elem) {
    let attr = $(elem).parents(".list").attr("data-id");
    parse = parse.filter(function (elem) {
        return elem.id != attr;
    });
    let s = JSON.stringify(parse);
    cookies.set("shop", s);
}

/**
 * remove List 按钮删除选中的li 为每个选中的按钮绑定删除事件
 * @param elem 选择删除按钮
 */
function removeList(elem) {
    for (const elemElement of elem) {
        elemElement.addEventListener('click', function (e) {
            cookiesRemove(this);
            $(this).parents(".list").remove();
            productNumber.html($("#shopList .list").length);
            if (shop!=''){
                let length = JSON.parse(cookies.get("shop")).length;
                $("#carCount").html(length);
            }else {
                $("#carCount").html(0);
            }
            getSumMoney();  //获得商品总价
        })
    }
}

/**
 * getSumMoney 获得购物车商品的总价
 */
function getSumMoney() {
    let shop = cookies.get("shop");
    let parse = JSON.parse(shop);
    let sum = 0
    //
    let filter = $(".checkIt").filter(function (i,e){
        return $(e).prop("checked")==true;
    });

    let map = filter.map(function (i, e){
        return $(e).parents(".list").attr("data-id");
    });
    map = Array.from(map);
    let f = parse.map(function (e,i){
            return map.indexOf(e.id)!=-1 ? e:false;
    });

    f.forEach(function (elem) {
        if (elem){
            sum += parseFloat(elem.price) * parseInt(elem.number);
        }
    })
    $("#sumMoney").html(sum.toFixed(2));
}

/**
 * 为每个需要获得重新获得总价元素的checkbox绑定事件
 * @param elem 选择产品的checkedbox
 */
function getCheckedSum(elem){
    for (const elemElement of elem) {
        elemElement.addEventListener('click',function (e){
            getSumMoney();
            if (!$(this).prop("checked")){
                $("#all").prop({checked:false})
            }
        })
    }
}




all[0].addEventListener('click',function (e){

    if ($(this).prop("checked")==false){
        checkIt.each(function (i,e){
            $(e).prop({checked:false})
        })
        getSumMoney();
    } else {
        checkIt.each(function (i,e){
            $(e).prop({checked:true})
        })
        getSumMoney();
    }

})


