'use strict';
const cookies = {
    get(key) {
        if (document.cookie) {
            let cookie = document.cookie.split("; ");
            let item;
            for (let string of cookie) {
                item = string.split("=");
                if (item[0] == key) {
                    return item[1];
                }
            }
        }
        return '';
    },
    set(key, value, time) {
        if (typeof time == 'time') {
            let d = new Date();
            d.setDate(d.getDate() + time);
            document.cookie = `${key}=${value};expires={d};"/"`;
        } else {
            document.cookie = `${key}=${value};"/"`;
        }
        return this
    },
    remove(key) {
        if (key instanceof Array) {
            console.log(key)
            for (const keyElement of key) {
                this.set(keyElement, '', -1);
            }
        } else {
            this.set(key, '', -1);
        }
    }

}


export default cookies;