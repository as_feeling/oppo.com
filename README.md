# oppo.com

这个项目是oppo购物商城的一个小案例

### 项目说明

    1.首页：基本首页布局  js内容为每日抢购的定时器功能  console.log中显示为可购买的产品列表 ajax导入
    2.产品页面：ajax 图片、后端数据导入，做了简易加入购物车和购买的一些逻辑验证 不仅限于加入购物车的数量、
			  input可修改的数量 完全依赖于后端数据反馈 不依赖于页面dom 加入购物车或购买添加的用户验
			  证，采用了cookies的形式，依赖于cookies中用户名的存在形式，而产品页依赖于后端的数据
			  图片滑动逻辑功能判断、模态框展示
    3.登录页面：基本的登录逻辑功能判断，简化了很多原有的页面登录功能模块 配合相关dom展示
    4.注册页面：基本注册页面逻辑布局，国家选择采用了bootstrap-select选择模块，可搜索国家来选择，手机号
			  和密码采用了正则判断，依照了原网页的逻辑判断实现
    5.购物车页面：涵盖了全部逻辑代码实现，边界值，NAN值，删除，全选、单选、总价、单价的实现功能
    
    注意：进入购物车页面和在产品页面中加入购物车或购买需要首先登录你的用户名，如果想测试的话可以自行在
		cookies中加入用户名等信息
    
    common.js 中主要操作了头部的信息，包括但不限于显示登录者的购物车数量、是否登
        陆、跳转指令等..
    
    一阶段版本为 V1.3
    试验项目网站：https://gogofeeling.space/
    用户名:root123
    密码:root
    因为本人给购物车做了蕾丝登录拦截功能，所需需要用户名，关于cookies方面，有点bug需要，可以在首页的情
    况下自行在控制台中输入cookies username:your username,password:your password 来进行购物车
    页面的测试。首页右上角的购物车哟点bug  某些导航类功能在本地设置好了，但是挪到服务器可能存在没有修改
    到位的情况。
    思考：可能不应该用cookides来存取这类信息


前端(PC)端项目
- 项目名 oppo.com
    - [node_modules]   存放第三方node模块 由npm自动创建
    - [interface]      存放php接口文件
        - [library]    php库文件 conn
    - [src]            存放源代码(开发环境代码)
        - [html]
            - index.html
            - product.html
            - shoppingCar.html
            - registered.html
            - login.html
        - [js]
          -[lib]  存放前端库文件(模块)
          - jquery.js
          - bootstrap*.js
          - cookie.js
          - conutrypicker.min.js 国家列表
		  - common.js 公共js
		  - select.js
            - index.js
            - product.js
            - login.js
			- registered.js
			- shoppingCar.js
        - [styles]
            - index.less
            - product.less
			- login.less
			- registered.less
			- shoppingCar.less
			- bootstrap*.less
			- common.less
        - [imgs] 项目首页图片
			- priduct-list ajax返回的图片
    - [dist]           存放部署环境代码(工具生成的代码) grunt gulp webpack ...  还没用到
        - [html]
            - index.html
            - product.html
        - [js]
            - [library]
                - jquery.min.js
                - bootstrap.min.js
                - cookie.min.js
                - querystring.min.js
            - index.min.js
            - product.min.js
        - [css]
            - index.min.css
            - product.min.css
        - [imgs]
    - README.md        项目说明
    - .gitignore       git忽略文件
    - package.json     初始化项目时自动生成(npm init -y)
    - gulpfile.js      gulp工具配置文件







-------------------
在将目录结构创建好以后
1. 填写一个git忽略文件
2. 初始化项目(在项目根目录执行)   `$ npm init -y`
3. 初始化代码管理工具 `$ git init`

-------------------
### 项目构建工具 gulp
Gulp.js 是基于nodejs 构建的, 它利用了Nodejs的流控制能力 辅助项目构建.
你可以快速通过Gulp构建项目 减少频繁的I/O操作.

项目的代码 划分成三类
1. 源代码(用于开发环境)
2. 第三方代码(jquery bootstrap)
3. 部署代码(用于部署服务器环境的代码 由工具生成)


### nrm 工具
npm(nodejs包管理工具库) 它的服务器在国外 访问速度慢
使用国内镜像提升访问速度
国内的npm镜像由阿里云免费提供

两种使用方式
1. 使用 nrm 工具切换镜像源
```bash
$ npm install nrm -g   # 全局安装nrm
$ nrm ls               # 查看所有镜像源
$ nrm use taobao       # 切换到taobao镜像
```

2. 使用cnpm工具代替npm工具 (推荐)
```bash
$ npm install cnpm -g --registry=https://registry.npm.taobao.org/
```

### 第三方工具(库) 安装
```bash
### 安装
$ cnpm install[i] package[@verstion] -g             # 全局安装(命令行工具)
$ cnpm install[i] package[@verstion] --save[-S]     # 项目依赖安装(项目中需要用到的代码)
$ cnpm install[i] package[@verstion] --save-dev[-D] # 项目开发依赖安装(开发工具)
```

使用 npm/cnpm 安装依赖时 需要在项目的根目录执行命令
osx用户进行全局安装时 需要获得超级管理员权限 `sudo -s`

```bash
$ cnpm uninstall pakcage --save   # 卸载模块
```

### gulp安装
```bash
# 全局安装的东西 每台电脑只需要进行一次安装
$ cnpm i gulp -g      # 全局安装gulp
$ cnpm i gulp-cli -g  # 全局安装gulp命令行工具
$ cnpm i gulp -D      # 项目开发依赖安装gulp(必须在项目根目录执行)
```

### Command line instructions
###  You can also upload existing files from your computer using the instructions below.

### Git global setup
```bash
git config --global user.name "shen zuyao"
git config --global user.email "szy947224730@gmail.com"
```

### Create a new repository
```bash
git clone git@gitlab.com:as_feeling/123.git
cd 123
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

### Push an existing folder
```bash
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:as_feeling/123.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

### Push an existing Git repository
```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:as_feeling/123.git
git push -u origin --all
git push -u origin --tags
```


1. 电商网站(如  京东、天猫、苏宁易购 ,小米,淘宝,oppo,华为)
2. 页面( 网站首页4屏 注册页 登陆页 商品详情页 购物车页 )
3. 前后端全站PC端项目 不考虑移动端（ 可以使用bootstarp ）
4. 功能实现( 首页所有可见效果 注册和登陆实现功能和表单验证 商品详情准备好静态页面 购物车页面准备好静态页面 )
5. 所有前端JavaScript代码 采用 ES6模块规范
6. 前后端分离( 分开文件目录 后端仅提供数据接口 采用php )
7. 所有的样式采用less