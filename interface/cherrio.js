const path = require('path');
const https = require('https');
const fs = require('fs');
const cheerio = require('cheerio');


https.get('https://www.jd.com/', (res) => {
    const {statusCode} = res;
    const contentType = res.headers['content-type'];

    let error;
    // Any 2xx status code signals a successful response but
    // here we're only checking for 200.
    if (statusCode !== 200) {
        error = new Error('Request Failed.\n' +
            `Status Code: ${statusCode}`);
    } else if (!/^text\/html/.test(contentType)) {
        error = new Error('Invalid content-type.\n' +
            `Expected text/html but received ${contentType}`);
    }
    if (error) {
        console.error(error.message);
        // Consume response data to free up memory
        res.resume();
        return;
    }
    res.setEncoding('utf8');
    let rawData = '';
    res.on('data', (chunk) => {
        rawData += chunk;
    });
    res.on('end', () => {
        // console.log(rawData);
        // fs.writeFile(path.join(__dirname,'index.html'),rawData,'utf-8',err => {
        //     if (err) throw  err;
        // })
        let $ = cheerio.load(rawData);
        let reg = /^(\/\/)|(http:\/\/)/;
        $('img').each((i, e) => {
            let url = $(e).attr('src').replace(reg, 'https://');
            let filename = url.split('/').pop();
            https.get(url, res => {
                let imgData = '';
                res.setEncoding('binary');
                res.on('data', chunk => imgData += chunk);
                res.on('end', () => {
                    console.log(url)
                    fs.writeFile(path.join(__dirname, 'img', filename), imgData, 'binary', err => {
                            if (err) throw err;
                            console.log('down');
                        }
                    );
                });
            });
        });
    });
}).on('error', (e) => {
    console.error(`Got error: ${e.message}`);
});

// Create a local server to receive data from
const server = https.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(JSON.stringify({
        data: 'Hello World!'
    }));
}).listen(8080, () => {
    console.log('works')
});